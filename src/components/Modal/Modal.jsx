import styles from './Modal.module.scss'
import ModalButton from './ModalBtn'

function Modal({
    active,
    header,
    closeModal,
    children,
    text
    }){

    return (
        <div className={active? styles.active : styles.hidden} onClick={closeModal}>
            <div className={styles.modal__content} onClick={ event => event.stopPropagation()}>
                <div className={styles.modal__header}>
                    <span className={styles.modal__header_text}>{header}</span>
                    <button className={styles.close} onClick={closeModal}>X</button>

                </div>
                <span className={styles.modal__text}>{text}</span>
                <div className={styles.modal__footer}>
                <ModalButton actions={children} />
                </div>
            </div>
        </div>
)}

export default Modal