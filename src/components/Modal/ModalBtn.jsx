
import styles from './Modal.module.scss'

function ModalButton({actions}){
    return <button className={styles.modal__btn_wrapper}>{actions}</button>
}

export default ModalButton