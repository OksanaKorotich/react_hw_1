import styles from './Button.module.scss'

function Button({ backgroundColor, text, onClick }){
    return <button
    className = {styles.btn}
    style = {{backgroundColor: backgroundColor, borderColor: backgroundColor}}
    onClick={onClick}
    >{text}</button>
}

export default Button