
import Button from "./components/Button/Button";
import Modal from "./components/Modal/Modal";
import styles from './components/Modal/Modal.module.scss'
import { useState } from "react";


function App() {

  const [showModal, setShowModal] = useState({
    firstModal: false,
    secondModal: false,
  });

  const closeModal = () => {
    setShowModal({ firstModal: false, secondModal: false });
  };

  return (

    <div className="app">
      <>
      <Button
          text = 'Open first modal'
          backgroundColor = '#a98c89'
          onClick={() => {
            setShowModal({ ...showModal, firstModal: true });
          }}
          />

          <Button
          text = 'Open second modal'
          backgroundColor = '#00ccbe'
          onClick={() => {
            setShowModal({ ...showModal, secondModal: true });
          }}
          />
      </>

      <Modal
      header="Do you want to delete this file?"
      text = 'Once you delete this file, it won`t be possible to undo this action. Are you sure you want to delete it?'
      active={showModal.firstModal}
      closeModal={closeModal}
      >
        <button className={styles.modal__action_btn} onClick={closeModal}>OK</button>
        <button className={styles.modal__action_btn} onClick={closeModal}>Cancel</button>
      </Modal>

      <Modal
      header="Do you want to add the product to favorites?"
      text = 'Add the product now so you don`t forget it. You will be able to easily find it to place your order later.'
      active={showModal.secondModal}
      closeModal={closeModal}
      >
        <button className={styles.modal__action_btn} onClick={closeModal}>ADD</button>
        <button className={styles.modal__action_btn} onClick={closeModal}>Cancel</button>
      </Modal>
    </div>
  );
}

export default App;
